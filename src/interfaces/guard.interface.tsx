/**
 * Interface for modelling guards as in the Vivian Framework format
 */
export interface Guard {
    // Runtime variables
    // Dimensions of touchable image, needed to calculate regions
    ImageWidth: number,
    ImageHeight: number,

    // Export variables
    EventParameter: "TOUCH_X_COORDINATE" | "TOUCH_Y_COORDINATE",
    Operator: "LARGER" | "SMALLER",
    CompareValue: number

}


