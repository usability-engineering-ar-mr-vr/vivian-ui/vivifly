import { ElementType } from "../types/element-type.type";
import {Vector3} from "./vector3.interface";

/**
 * Class for modelling interaction elements in the Vivian state machine format
 */
export interface InteractionElement {
    Name: string,
    Type: ElementType

    // Screen specific values
    Plane?: Vector3
}