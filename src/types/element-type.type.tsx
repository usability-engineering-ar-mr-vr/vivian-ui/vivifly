
/**
 * Type to define the type of an element
 */
export type ElementType = "Screen" | "Light" | "Button" | "TouchArea"

export const ELEMENT_TYPE_SCREEN: ElementType = "Screen"
export const ELEMENT_TYPE_LIGHT: ElementType = "Light"
export const ELEMENT_TYPE_BUTTON: ElementType = "Button"
export const ELEMENT_TYPE_TOUCH_AREA: ElementType = "TouchArea"