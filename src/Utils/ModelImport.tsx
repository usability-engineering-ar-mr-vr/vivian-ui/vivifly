import { AppContext } from '../interfaces/app-context.interface'
import JSZip, { JSZipObject } from 'jszip'
import { VISUALIZATION_TYPE_SCREEN, VISUALIZATION_TYPE_FLOAT } from '../types/visualization-type.type'
import { ContextUtils } from './ContextUtils'
import { VisualizationCondition } from '../interfaces/visualization-condition.interface'
import { State } from '../interfaces/state.interface'
import { Transition } from '../interfaces/transition.interface'

/**
 * Class to import models
 */
export class ModelImport {

    private context: AppContext

    /**
     * Creates a new ModelImport instance
     * @param context context that is used for the import
     */
    constructor(context: AppContext) {
        this.context = context;
    }

    /**
     * Converts the current context of the object to zip without the model and provides it as base 64 encoded string
     */
    importModel(file: File) {
        if (file.type === "application/x-zip-compressed") {
            const modelImport = this;
            JSZip.loadAsync(file).then(function (zip) {
                modelImport.startModelUpload(zip);
            });
        }
        else {
            this.context.unityWrapper.uploadModel(file.name, file);
        }
    }

    /**
     * Converts the current context of the object to zip and starts zip download
     */
     async startModelUpload(zip: JSZip) {
        await this.loadInteractions(zip);
        await this.loadVisualizations(zip);
        var states = await this.loadStates(zip);
        await this.loadTransitions(zip, states);

        this.loadModel(zip);
    }

    /**
     * loads the model into the unity context
     */
    private async loadModel(zip: JSZip) {
        const modelCandidates = new Array();
        zip.forEach(function (relativePath, file) {
            if (!file.dir && (relativePath.indexOf('/') < 0)) {
                modelCandidates.push(file.name);
            }
        });

        if (modelCandidates.length == 0) {
            throw "the zip file does not contain a valid model file";
        }
        else if (modelCandidates.length > 1) {
            throw "the zip file contains more than one model candidate";
        }
        else {
            const zblob = await zip.file(modelCandidates[0])?.async("blob");

            if (!zblob) {
                throw "could not read the model file " + modelCandidates[0];
            }
            else {
                this.context.unityWrapper.uploadModel(modelCandidates[0], zblob);
            }
        }
    }

    /**
     * Converts the interaction elements in the zip file to load them in the current context
     */
    private async loadInteractions(zipFile: JSZip) {
        // read interaction Specification
        var jsonFile = zipFile.file("FunctionalSpecification/InteractionElements.json");

        if (!jsonFile) {
            throw "the zip file does not contain a \"FunctionalSpecification/InteractionElements.json\" file";
        }

        let obj = JSON.parse(await jsonFile.async("string"));

        this.context.interactionElements = obj.Elements;
    }

    /**
     * Converts the visualization elements in the zip file to load them in the current context
     */
    private async loadVisualizations(zipFile: JSZip) {
        // read visualization Specification
        var jsonFile = zipFile.file("FunctionalSpecification/VisualizationElements.json");

        if (!jsonFile) {
            throw "the zip file does not contain a \"FunctionalSpecification/VisualizationElements.json\" file";
        }

        let obj = JSON.parse(await jsonFile.async("string"));

        this.context.visualizationElements = obj.Elements;
    }

    /**
     * Converts the states in the zip file to load them in the current context
     */
    private async loadStates(zipFile: JSZip): Promise<Array<State>> {
        // read visualization Specification
        var jsonFile = zipFile.file("FunctionalSpecification/States.json");

        if (!jsonFile) {
            throw "the zip file does not contain a \"FunctionalSpecification/States.json\" file";
        }

        let obj = JSON.parse(await jsonFile.async("string"));

        let newStates: Array<State> = new Array();

        if (obj.States) {
            for (var i = 0; i < obj.States.length; i++) {
                var correctState: State = { Name: obj.States[i].Name, id: i };
    
                // Replace image file names with actual images
                correctState.Conditions = new Array();
                
                if (typeof obj.States[i].Conditions) {
                    for (var j = 0; j < obj.States[i].Conditions.length; j++) {
                        var condition = obj.States[i].Conditions[j];
                        if ((condition.Type === VISUALIZATION_TYPE_SCREEN) && (!condition.File))
                        {
                            var imageFile = await zipFile.file("Screens/" + condition.FileName)?.async("blob");
                            correctState.Conditions.push({...condition, File: imageFile, FileName: condition.FileName});
                        } else {
                            correctState.Conditions.push(condition);
                        }
                    }
                }
    
                newStates.push(correctState);
            }
        }

        if (newStates.length > 0) {
            newStates[0].isStart = true;
        }

        this.context.states = newStates;

        return newStates;
    }

    /**
     * Converts the transitions in the zip file to load them in the current context
     */
     private async loadTransitions(zipFile: JSZip, states: Array<State>) {
        // read visualization Specification
        var jsonFile = zipFile.file("FunctionalSpecification/Transitions.json");

        if (!jsonFile) {
            throw "the zip file does not contain a \"FunctionalSpecification/Transitions.json\" file";
        }

        let obj = JSON.parse(await jsonFile.async("string"));

        let newTransitions: Array<Transition> = new Array();

        if (obj.Transitions) {
            for (var i = 0; i < obj.Transitions.length; i++) {
                var transition = obj.Transitions[i];
                console.log(transition.SourceState);
                newTransitions.push({
                    ...transition,
                    SourceStateID: ContextUtils.getSituationId(transition.SourceState, states),
                    SourceState: undefined,
                    DestinationStateID: ContextUtils.getSituationId(transition.DestinationState, states),
                    DestinationState: undefined,
                    Guards: transition.Guards,
                    IsTemp: (transition.Timeout)
                });
            }
        }

        this.context.transitions = newTransitions;
    }
}