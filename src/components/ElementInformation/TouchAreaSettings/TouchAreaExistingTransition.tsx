import React from 'react'
import SituationSelect from '../../Core/SituationSelect'
import { State } from '../../../interfaces/state.interface'
import { Transition } from '../../../interfaces/transition.interface'

/**
 * Type for props
 */
type PropsType = {
    newSituationTouchAreaClicked: () => void,
    changeDestination: (destinationSituationID: number | undefined) => void,
    states: State[],
    transition?: Transition | undefined,
    currentDestinationId?: number | undefined,
    isDisabled?: boolean
}

/**
 * Settings to change the existing transition of a button
 */
export default class TouchAreaExistingTransition extends React.Component<PropsType> {

    // TODO: (optional) Add goto situation button
    /**
     * Sets a new destination for the button in the current situation
     * @param situationID new chosen situation
     */
    setDestination(situationID: number | "new" | undefined) {
        if (situationID === "new") {
            this.props.newSituationTouchAreaClicked()
        } else {
            this.props.changeDestination(situationID)
        }
    }

    render() {
        const DestinationState = this.props.states.find(state => state.id === this.props.transition?.DestinationStateID)
        return <>
            <div>Touching this touch area in the current situation leads to </div>
            <SituationSelect
                selectSituation={this.setDestination.bind(this)}
                possibleStates={this.props.states}
                situationID={this.props.currentDestinationId !== undefined ? this.props.currentDestinationId : DestinationState?.id}
                emptyChoiceAllowed={true}
                isDisabled={this.props.isDisabled}
            />
        </>
    }
}