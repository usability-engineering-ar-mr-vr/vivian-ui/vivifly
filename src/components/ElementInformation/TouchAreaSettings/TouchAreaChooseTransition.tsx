import React from 'react'
import { MDBIcon, MDBBtn } from 'mdbreact'

// Type for props
type PropsType = {
    newSituationTouchAreaClicked: () => void,
    existingSituationTouchAreaClicked: () => void
    existingSituationTouchAreaDisabled: boolean
}

/**
 * Component to choose whether the current touch area should lead to a new situation or an already existing one
 * @param props 
 */
export default function TouchAreaChooseTransition(props: PropsType) {
    return <>
        <div>Pushing a touch area leads to another situation, e.g. lights turned on or off.<br />
            Where should this touch area lead to?</div>
        <div className="row">
            <div className="col-5 px-2">
                <MDBBtn color="success" className="d-flex align-items-center py-2 px-4 w-100"
                    onClick={props.newSituationTouchAreaClicked}>
                    <MDBIcon icon="plus-square" size="lg" />
                    <div className="d-inline-block ml-3 text-left">New<br />Situation</div>
                </MDBBtn>
            </div>
            <div className="col-5 px-2">
                <MDBBtn color="info" className="d-flex align-items-center py-2 px-4 w-100"
                    disabled={props.existingSituationTouchAreaDisabled}
                    onClick={props.existingSituationTouchAreaClicked}>
                    <MDBIcon icon="random" size="lg" />
                    <div className="d-inline-block ml-3 text-left">Existing<br />Situation</div>
                </MDBBtn>
            </div>
        </div>
    </>
}