import React from 'react'
import { MDBIcon, MDBBtn } from 'mdbreact'
import {ContextUtils} from "../../../Utils/ContextUtils";
import {ELEMENT_TYPE_SCREEN} from "../../../types/element-type.type";
import {VisualizationElement} from "../../../interfaces/visualization-element.interface";
import {Transition} from "../../../interfaces/transition.interface";

// Type for props
type PropsType = {
    element: string,
    transitions: Transition[],
    visualizationElements: VisualizationElement[] | undefined,
    noScreenImage: boolean,
    chooseImageAreasTouchAreaClicked: () => void,
    addDisplayType: () => void
}

/**
 * Component to choose whether the current touch area should lead to a new situation or an already existing one
 * @param props 
 */
export default function TouchAreaChooseAreas(props: PropsType) {
    return <>
        {props.transitions.length >= 1 ?
            <div>There is at least one touchable area that leads to a different situation. You can edit the touchable areas: </div>
        :
            <div>If the current element is also defined as a display, you can define certain areas
            of its image as touchable (e.g. buttons) that can later lead to other situations.</div>}
        <div className="row">
            <div className="col-5 px-2">
                <MDBBtn color="primary" className="d-flex align-items-center py-2 px-4 w-100"
                        disabled={props.noScreenImage}
                        onClick={props.chooseImageAreasTouchAreaClicked}>
                    <MDBIcon icon="mouse-pointer" size="lg" />
                    <div className="d-inline-block ml-3 text-left">Define<br />Image Areas</div>
                </MDBBtn>
            </div>
        </div>
        {props.visualizationElements &&
        !ContextUtils.elementHasVisualizationType(props.element, ELEMENT_TYPE_SCREEN, props.visualizationElements) ?
            <div>You can add a display effect by clicking <a href={"#"} onClick={props.addDisplayType}>here</a>.</div> : null}
    </>
}