import React, { Component } from 'react'
import { MDBIcon } from 'mdbreact'
import TouchAreaChooseTransition from './TouchAreaChooseTransition'
import TouchAreaExistingTransition from './TouchAreaExistingTransition'
import { ContextUtils } from '../../../Utils/ContextUtils'
import {ELEMENT_TYPE_SCREEN, ELEMENT_TYPE_TOUCH_AREA} from '../../../types/element-type.type'
import ReactTooltip from 'react-tooltip'
import { Actions } from '../../../interfaces/actions.interface'
import { Transition } from '../../../interfaces/transition.interface'
import { State } from '../../../interfaces/state.interface'
import {VisualizationElement} from "../../../interfaces/visualization-element.interface";
import TouchAreaChooseAreas from "./TouchAreaChooseAreas";

type PropsType ={
    actions: Actions,
    currentSituationID: number,
    element: string,
    transitions: Transition[],
    states: State[],
    visualizationElements?: VisualizationElement[]
}

/**
 * Component to display the settings for button elements
 */
export default class TouchAreaSettings extends Component<PropsType> {

    /**
     * Function that gets called if the new situation button is pressed
     * Creates a new situation and sets a transition from the current button to this situation
     * Opens the naming modal
     */
    newSituationButtonClicked() {
        const newSituationID = this.props.actions.createNewSituation("")
        this.props.actions.setTouchAreaTransition(
            this.props.currentSituationID,
            newSituationID,
            this.props.element)
        this.props.actions.setRenamingModalSituation(newSituationID)
    }

    /**
     * Function that gets called if the choose image areas button is pressed
     * Opens the area selection modal
     */
    chooseImageAreasTouchAreaClicked() {
        this.props.actions.setChooseImageAreasModalVisibility(true)
    }

    /**
     * Function that gets called if the existing situation button is pressed
     * Creates a new transition without destination
     */
    existingSituationButtonClicked() {
        this.props.actions.setTouchAreaTransition(
            this.props.currentSituationID,
            undefined,
            this.props.element)
    }

    /**
     * Adds the display type to the current element
     */
    addDisplayType() {
        this.props.actions.addElementType(this.props.element, ELEMENT_TYPE_SCREEN)
    }

    render() {
        const transitions = ContextUtils.getTransitions(this.props.element, this.props.currentSituationID, this.props.transitions, true)
        return <>
            <div className="row">
                <div className="col-1 p-0 d-flex justify-content-center align-items-center">
                    <MDBIcon icon="fingerprint" size="lg" className="light-green-text" />
                </div>
                <div className="col-11">
                    <div className="mb-1" >
                        <div className="d-inline">TouchArea</div>
                        <div className="d-inline-block float-right">
                            <MDBIcon far icon="trash-alt"
                                className="mx-2 hover-icon"
                                data-tip="Remove touch area effect"
                                data-for="element-button-actions"
                                onClick={() => this.props.actions.removeElementType(this.props.element, ELEMENT_TYPE_TOUCH_AREA)}
                            />
                            <ReactTooltip place="bottom" effect="solid" id="element-button-actions" />
                        </div>
                    </div>

                    <div className="card-text">
                        {!transitions.length ?
                            <TouchAreaChooseTransition
                                newSituationTouchAreaClicked={this.newSituationButtonClicked.bind(this)}
                                existingSituationTouchAreaClicked={this.existingSituationButtonClicked.bind(this)}
                                existingSituationTouchAreaDisabled={!!this.props.states && this.props.states.length <= 1} />
                        : transitions.length === 1 && !transitions[0].Guards?.length?
                            <TouchAreaExistingTransition
                                transition={transitions[0]}
                                states={this.props.states}
                                changeDestination={(destination: number | undefined) => transitions[0].SourceStateID !== undefined && !!transitions[0].InteractionElement
                                    && this.props.actions.setTouchAreaTransition(transitions[0].SourceStateID, destination, transitions[0].InteractionElement)}
                                newSituationTouchAreaClicked={this.newSituationButtonClicked.bind(this)} />
                        : null}
                        <TouchAreaChooseAreas
                            element={this.props.element}
                            transitions={transitions}
                            visualizationElements={this.props.visualizationElements}
                            noScreenImage={ContextUtils.getScreenImage(this.props.element, this.props.currentSituationID, this.props.states) === undefined}
                            chooseImageAreasTouchAreaClicked={this.chooseImageAreasTouchAreaClicked.bind(this)}
                            addDisplayType={this.addDisplayType.bind(this)}/>
                    </div>
                </div>
            </div>
        </>
    }
}
