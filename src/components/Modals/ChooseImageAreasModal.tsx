import React from 'react'
import Modal from './Modal'
import {MDBBtn, MDBIcon} from 'mdbreact'
import { Actions } from '../../interfaces/actions.interface'
import RegionSelect from 'react-region-select';
import {Transition} from "../../interfaces/transition.interface";
import TouchAreaExistingTransition from "../ElementInformation/TouchAreaSettings/TouchAreaExistingTransition";
import {State} from "../../interfaces/state.interface";

type PropsType = {
    states: State[],
    currentSituationID: number
    actions: Actions,
    element: string,
    displayImage: File | undefined,
    transitions: Transition[]
}

interface IState {
    regions: any[],
    selectedRegion: any
}

/**
 * Component to display the modal to define regions on a touchable display
 */
export default class ChooseImageAreasModal extends React.Component<PropsType, IState> {
    constructor (props: PropsType) {
        super(props);
        this.regionRenderer = this.regionRenderer.bind(this);
        this.onChange = this.onChange.bind(this);
        this.imgDimensions = React.createRef()

        this.defaultRegionStyle = {
            background: 'rgb(255,75,0,0.2)',
            border: 'solid rgb(255,75,0,1) 1px',
            outline: 'solid rgb(255,75,0,1) 1px',
        }
        this.selectedRegionStyle = {
            background: 'rgb(255,75,0, 0.5)',
            border: 'solid rgb(255,75,0,1) 2px',
            outline: 'solid rgb(255,75,0,1) 2px',
        }
        this.invisibleRegionStyle = {
            display: 'none'
        }

        // If the current situation already has defined areas and transitions, recreate them
        let regions: any = []
        this.props.transitions.map((transition, index) => {
            if (!transition.Guards) return
            let gx: number = 0, gy: number = 0, gwidth: number = 0, gheight: number = 0
            transition.Guards?.map(guard => {
                if (guard.EventParameter === "TOUCH_X_COORDINATE" && guard.Operator === "LARGER") gx = (guard.CompareValue / guard.ImageWidth) * 100
                else if (guard.EventParameter === "TOUCH_Y_COORDINATE" && guard.Operator === "LARGER") gy = (guard.CompareValue / guard.ImageHeight) * 100
                else if (guard.EventParameter === "TOUCH_X_COORDINATE" && guard.Operator === "SMALLER") gwidth = ((guard.CompareValue - ((gx/100) * guard.ImageWidth)) / guard.ImageWidth) * 100
                else if (guard.EventParameter === "TOUCH_Y_COORDINATE" && guard.Operator === "SMALLER") gheight = ((guard.CompareValue - ((gy/100) * guard.ImageHeight)) / guard.ImageHeight) * 100
            })
            regions.push({
                x: gx,
                y: gy,
                width: gwidth,
                height: gheight,
                new: false,
                data: {
                    index: index,
                    sourceStateID: transition.SourceStateID,
                    destinationStateID: transition.DestinationStateID,
                    isTemp: transition.IsTemp,
                    regionStyle: this.isContextTemp()?
                        (!transition.IsTemp? this.invisibleRegionStyle : index === this.props.transitions.length - 1 ? this.selectedRegionStyle : this.defaultRegionStyle)
                        : (index === 0 ? this.selectedRegionStyle : this.defaultRegionStyle)
                },
                isChanging: false
            })
        })
        this.state = {regions: regions, selectedRegion: this.isContextTemp()? regions.length - 1 : 0};
    }

    /*========== Properties ==========*/

    defaultRegionStyle: {
        border: string;
        outline: string;
        background: string
    }

    selectedRegionStyle: {
        border: string;
        outline: string;
        background: string
    }

    invisibleRegionStyle: {
        display: string
    }

    imgDimensions: any;

    /**
     * Returns whether there are existing temporary transitions
     */
    isContextTemp() {
        return this.props.transitions.filter(t => t.IsTemp).length
    }

    /*========== Region Events ==========*/
    onChange (regions: any, index:number) {
        let newRegions = this.selectRegion(regions, index)
        this.setState({
            regions: newRegions,
            selectedRegion: index
        })
    }

    regionRenderer (regionProps: any) {
        if (!regionProps.isChanging) {
            return (
                <div>
                    <div style={{ position: 'absolute', right: "-1em", top: "-1em" }}>
                        <MDBBtn onClick={() => this.deleteRegion(regionProps.index)}
                            color={"light"}
                            style={{ borderWidth:1,
                                width:25,
                                height:25,
                                borderRadius:100,
                                padding: 0,
                            }}
                        >
                            <MDBIcon icon="trash" size="1x" />
                        </MDBBtn>
                    </div>
                </div>
            );
        }
    }

    /*========== Region selection methods ==========*/

    /**
     * Deletes the region associated to the delete button
     */
    deleteRegion (index: number) {
        // Reassign indexes to regions to keep consistency
        let regions = [...this.state.regions.slice(0, index), ...this.state.regions.slice(index + 1)]
            .map((region: any, i) => {
                return {...region, data: {...region.data, index: i}}
            })
        this.onChange(regions, 0)
    }

    /**
     * Changes the style of the currently selected region
     */
    selectRegion(regions: any, index: number) {
        let newRegions: any = regions.map((region: any) => {
            // set style of selected region
            let regionStyle
            if(region.data.index === index)
                regionStyle = this.selectedRegionStyle
            else
                regionStyle = this.defaultRegionStyle
            if(region.data.regionStyle === this.invisibleRegionStyle)
                regionStyle = this.invisibleRegionStyle

            return {...region, data: {...region.data, regionStyle: regionStyle}}
        })
        // update state
        return newRegions
    }

    /*========== Modal methods ==========*/

    /**
     * Function that gets called if the new situation button is pressed
     * Creates a new situation and sets a transition from the current button to this situation
     * Opens the naming modal
     */
    newSituationButtonClicked() {
        // Create a new situation and set it as a destination for the currently selected region
        const newSituationID = this.props.actions.createNewSituation("")
        this.changeDestination(newSituationID)

        // Opens the situation naming modal. Saves the current work on this modal as temporary transitions
        // When returning to this modal, they are converted back to regions. If we cancel then, remove all transitions that are flagged as temp
        this.props.actions.addImageRegions(this.state.regions, this.props.currentSituationID, this.props.element,
            this.imgDimensions.current.naturalWidth, this.imgDimensions.current.naturalHeight, true)

        // Stay on the current situation after creating the new situation to keep correct values
        this.props.actions.setRenamingModalSituation(newSituationID, true)
    }

    /**
     * Function that gets called if an existing situation is chosen in the select
     * Adds transition data to the region object
     */
    changeDestination (destination: number | undefined) {
        if(this.props.currentSituationID !== undefined && !!this.props.element) {
            let selectedRegion: any = this.state.regions[this.state.selectedRegion]
            selectedRegion.data.sourceStateID = this.props.currentSituationID
            selectedRegion.data.destinationStateID = destination

            this.onChange([
                ...this.state.regions.slice(0, this.state.selectedRegion),
                selectedRegion,
                ...this.state.regions.slice(this.state.selectedRegion + 1)], this.state.selectedRegion)
        }
    }

    /**
     * Adds touchable areas data to the current element and closes the modal
     */
    saveAndClose() {
        let regions = this.state.regions
        if(this.isContextTemp()) {
            // If we have temp transitions and we validate, we discard the old transitions that were not flagged as temp
            regions = this.state.regions.filter(r => r.data.isTemp)
        }
        this.props.actions.addImageRegions(regions, this.props.currentSituationID, this.props.element,
            this.imgDimensions.current.naturalWidth, this.imgDimensions.current.naturalHeight, false)
        this.closeModal()
    }

    cancel() {
        // Remove temporary transitions if there were any
        this.props.actions.removeTempTransitions()
        this.closeModal()
    }

    /**
     * Closes the current modal
     */
    closeModal() {
        this.props.actions.setChooseImageAreasModalVisibility(false)
    }

    render() {
        return <>
            <Modal title="Define touchable image areas" onClose={this.cancel.bind(this)}>
                <div>Click and drag with your mouse to
                    define touchable areas on the image: </div>
                <RegionSelect
                    regions={this.state.regions}                // Regions data
                    regionStyle={this.selectedRegionStyle}       // Defines the style of the regions
                    regionRenderer={this.regionRenderer}        // Defines additional content (close button)
                    constraint={true}                           // Limit the regions inside the image borders
                    onChange={this.onChange}>
                        <img src={URL.createObjectURL(this.props.displayImage)}
                             ref={this.imgDimensions}
                             className="img-fluid mx-auto d-block"
                             alt={this.props.displayImage?.name}/>
                </RegionSelect>

                <TouchAreaExistingTransition
                    states={this.props.states}
                    currentDestinationId={!!this.state.regions[this.state.selectedRegion] ?
                        this.state.regions[this.state.selectedRegion].data.regionStyle !== this.invisibleRegionStyle ?
                        this.state.regions[this.state.selectedRegion].data.destinationStateID : undefined : undefined}
                    changeDestination={this.changeDestination.bind(this)}
                    newSituationTouchAreaClicked={this.newSituationButtonClicked.bind(this)}
                    isDisabled={!this.state.regions.filter(r => r.data.regionStyle !== this.invisibleRegionStyle).length}/>

                <div className="d-flex justify-content-between mt-2">
                    <MDBBtn
                        color="light"
                        onClick={this.cancel.bind(this)}>
                        Cancel
                    </MDBBtn>
                    <MDBBtn
                        color="primary"
                        onClick={this.saveAndClose.bind(this)}>
                        Save
                    </MDBBtn>
                </div>
            </Modal>
        </>
    }
}