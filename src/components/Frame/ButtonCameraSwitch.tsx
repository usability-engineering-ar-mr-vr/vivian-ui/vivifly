import React from 'react'
import {MDBIcon, MDBBtn, MDBTooltip} from 'mdbreact'
import {Actions} from "../../interfaces/actions.interface";

type PropsType = {
    actions: Actions
}

export default class ButtonCameraSwitch extends React.Component<PropsType> {

    state = {
        isCameraBlenderStyle: true
    }

    switchCameraBehaviour() {
        this.props.actions.switchCameraBehaviour();
        this.setState({isCameraBlenderStyle: !this.state.isCameraBlenderStyle});
    }

    renderBlenderControls(){
        return <>
            <b>Hold right click: </b> Orbit the camera around model<br/>
            <b>Mouse wheel: </b> zoom in and out
        </>
    }

    renderUnityControls(){
        return <>
            {/*<b>WASD: </b> Move the camera around<br/>*/}
            <b>Hold right click: </b> Rotate the camera<br/>
            <b>Mouse wheel: </b> zoom in and out
        </>
    }

    render() {
        return <>
            <div className="d-flex align-items-end">
                <MDBTooltip
                    placement="top"
                >
                    <MDBBtn color="primary" onClick={this.switchCameraBehaviour.bind(this)}>
                        <MDBIcon icon="camera" size="lg" />
                        <div className="d-inline-block ml-3 text-left">Switch Camera controls</div>
                    </MDBBtn>
                    <div className="text-center">
                        <b>Current control scheme: </b><br/>
                        {this.state.isCameraBlenderStyle ? this.renderBlenderControls() : this.renderUnityControls()}
                    </div>
                </MDBTooltip>
            </div>
        </>
    }
}