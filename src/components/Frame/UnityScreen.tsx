import React from 'react'
import Unity from "react-unity-webgl"
import LoadingOverlay from './LoadingOverlay'
import UploadModelOverlay from './UploadModelOverlay'
import { AppContext } from '../../interfaces/app-context.interface'

type PropsType = {
    appContext?: AppContext,
    modelWasUploaded: boolean,
    isCurrentlyUploading: boolean,
    renderTutorial: boolean
}
export default class UnityScreen extends React.Component<PropsType> {

    render() {
        if( !this.props.appContext){
            return null
        }
        return <>
            {this.props.modelWasUploaded || <UploadModelOverlay appContext={this.props.appContext} renderTutorial={this.props.renderTutorial} />}
            {this.props.isCurrentlyUploading && <LoadingOverlay message="Uploading model" renderTutorial={this.props.renderTutorial} />}
            <div className="position-absolute h-100 w-100 overflow-hidden">
                <div className="position-relative h-100 w-100">
                    <Unity  unityContext={this.props.appContext.unityWrapper.unityContent}
                            style={{
                              height: "100%",
                              width: "100%"
                            }}/>
                </div>
            </div>
        </>
    }
}
