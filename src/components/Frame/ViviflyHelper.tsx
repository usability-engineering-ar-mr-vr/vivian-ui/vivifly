import React from 'react'
import {
  MDBIcon,
  MDBBtn,
  MDBTooltip,
  MDBContainer,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText, MDBProgress, MDBCardHeader
} from 'mdbreact'
import StepWizard, { StepWizardProps } from "react-step-wizard";

/*let custom = {
    enterRight: '.animate__animated .animate__fadeInRight',
    enterLeft : '.animate__animated .animate__fadeInLeft',
    exitRight : '.animate__animated .animate__fadeOutRight',
    exitLeft  : '.animate__animated .animate__fadeOutLeft'
}*/

let lang = 'en';
let style = { minHeight: "30em" };

export default class ViviflyHelper extends React.Component {

  state = {
    SW: {
      currentStep: 0,
      totalSteps: 0,
      previousStep: undefined,
      nextStep: undefined
    }
  }

  updateSteps(stepChange: any) {
    this.setState({ ...this.state, currentStep: stepChange.currentStep });
  }

  setInstance(SW: StepWizardProps) {
    this.setState({ SW: SW });
  }

  // { SW } = state;

  render() {
    return <>
      <MDBContainer className={"position-fixed h-100 w-25 overflow-hidden elegant-color upload-overlay"}>
        <MDBCol>
          <MDBCard style={{ marginTop: "30px" }}>
            <MDBCardHeader color={"primary-color-dark"}>
              <MDBCardTitle>Tutorial</MDBCardTitle>
            </MDBCardHeader>
            <MDBCardBody>
              <h6>{lang == 'de' ? 'Schritt' : 'Step'} {this.state.SW.currentStep}/{this.state.SW.totalSteps}</h6>
              <MDBProgress value={100 * this.state.SW.currentStep / this.state.SW.totalSteps} className="my-2" />
              <br />
              <StepWizard instance={this.setInstance.bind(this)} onStepChange={this.updateSteps.bind(this)} initialStep={0} /*transitions={custom}*/>
                <Step1 />
                <Step2 />
                <Step3 />
                <Step4 />
                <Step5 />
                <Step6 />
                <Step7 />
                <Step8 />
                <Step9 />
                <Step10 />
                <Step11 />
                <Step12 />
              </StepWizard>
              <br />
              <div>
                <MDBBtn color="mdb-color" disabled={this.state.SW.currentStep === 1} onClick={this.state.SW.previousStep}>{lang == 'de' ? 'Zurück' : 'Previous'}</MDBBtn>
                <MDBBtn color="primary" disabled={this.state.SW.currentStep === this.state.SW.totalSteps} onClick={this.state.SW.nextStep}>{lang == 'de' ? 'Weiter' : 'Next'}</MDBBtn>
              </div>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBContainer>
    </>
  }
}

class Step1 extends React.Component {
  render() {
    if (lang == 'de') {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Dies ist eine Demonstration von Vivifly, einer Benutzeroberfläche für die Konfiguration von dynamischen Prototypen aus statischen Modellen.
          <br /><br />
          Dieses Tutorial führt Schritt für Schritt durch die Anwendung. Wenn du mit einem Schritt fertig bist, klicke auf Weiter.
          Wenn du zu einem abgeschlossenen Schritt  zurück gehen möchtest, kannst du dies über die Schaltfläche Zurück tun.
        </MDBCardText>
      </>
    }
    else {
      return <>
        <MDBCardText className="text-justify" style={style}>
          This demonstration showcases Vivifly, a user interface for configuring dynamic prototypes from static models.
          <br /><br />
          This tutorial will provide you instructions step by step to use the application. When you're done with a step, simply click next.
          Should you want to go back to review completed steps, click previous.<br />
        </MDBCardText>
      </>
    }
  }
}

class Step2 extends React.Component {
  render() {
    if (lang == 'de') {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Zunächst laden wir ein Modell hoch. Für dieses Tutorial werden wir eine einfache Mikrowelle verwenden, die du hier findest:<br />
          <a href="./Microwave.fbx" download>Modell herunterladen</a><br />
          <br />
          Sobald du das Modell heruntergeladen hast, kannst du es auf Vivifly hochladen. Dazu kannst du es entweder per Drag-and-Drop in die Upload-Zone ziehen oder auf die Upload-Schaltfläche klicken.
        </MDBCardText>
      </>
    }
    else {
      return <>
        <MDBCardText className="text-justify" style={style}>
          First, let's upload a model. For this tutorial, we'll be using a simple microwave that you can find here:<br />
          <a href="./Microwave.fbx" download>Download model</a><br />
          <br />
          Once you have downloaded the model, upload it on Vivifly by either dragging it into the upload zone, or by clicking the upload zone.
        </MDBCardText>
      </>
    }
  }
}

class Step3 extends React.Component {
  render() {
    if (lang == 'de') {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Vivifly hat 3 Hauptbereiche:
          <ul>
            <li>eine Navigationsleiste oben (Export Model, Run test)</li>
            <li>eine Zustandsliste links (Situations)</li>
            <li>das große Hauptfenster in der Mitte.</li>
          </ul>
          Das Modell, das du gerade hochgeladen hast, wird bereits im Hauptfenster angezeigt.<br /><br />
          Du kannst dich im virtuellen Raum umsehen, indem du die Kamera drehst. Halte dazu bitte die rechte Maustaste gedrückt, während du die Maus bewegst. Mit dem Mausrad kannst du zudem zoomen.<br /><br />
          Du kannst auch das Steuerungsschema ändern, indem du auf die dafür vorgesehene Schaltfläche in der unteren linken Ecke klickst ("Switch camera controls"). Dadurch wird die Kamera um das Modell gedreht, wenn du die rechte Maustaste gedrückt hältst.
        </MDBCardText>
      </>
    }
    else {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Vivifly has 3 main zones:
          <ul>
            <li>a top navbar (Export Model, Run test)</li>
            <li>a situation list on the left (Situations)</li>
            <li>and the main window in the center.</li>
          </ul>
          The model you just uploaded is displayed inside the main window.<br /><br />
          You can look around the virtual space by rotating the camera by holding down the right click, and zoom in and out with the mouse wheel.<br /><br />
          You can also change the control scheme by pressing the designated button in the bottom-left corner. This makes the camera rotate around the model when you hold down the right click.
        </MDBCardText>
      </>
    }
  }
}

class Step4 extends React.Component {
  render() {
    if (lang == 'de') {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Wenn du auf einen Teil des Modells klickst, wird dieser ausgewählt, und du wirst aufgefordert, die Art des ausgewählten Elements anzugeben. Die Karte auf der rechten Seite wird nachfolgend als Elementauswahl bezeichnet.<br /><br />
          Klicke auf die Schaltfläche "Erhitzen" der Mikrowelle (die quadratische Schaltfläche unten rechts), und definiere sie als "Button" (Schaltfläche) mit der Elementauswahl.<br /><br />
          Über das Speichern musst du dir keine Gedanken machen während du an der Elementauswahl arbeitest: Änderungen an der Elementauswahl werden automatisch gespeichert.
        </MDBCardText>
      </>
    }
    else {
      return <>
        <MDBCardText className="text-justify" style={style}>
          When clicking on a part of the model, you will isolate it, and you will be prompted to indicate the nature of the selected element. The card on the right is the element picker.<br /><br />
          Click on the microwave's "Heat" button (the square button on the bottom-right), and define it as a button with the element picker.<br /><br />
          Don't worry about saving changes while working on the element picker: they are saved automatically, so you won't loose anything by clicking out!
        </MDBCardText>
      </>
    }
  }
}

class Step5 extends React.Component {
  render() {
    if (lang == 'de') {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Jetzt wollen wir unserer Schaltfläche eine Aktion zuweisen. Wir möchten, dass die Mikrowelle das Essen erwärmt, wenn wir darauf drücken.<br /><br />
          Dazu klicken wir auf der Karte auf "New situation" (neuer Zustand). Daraufhin wird ein Dialogfenster angezeigt, in dem du den Namen des Zustands angeben kannst, z. B.: "Erhitzen".<br /><br />
          Nun wird unsere Mikrowelle von dem Start-Zustand in den Zustand "Erhitzen" übergehen, sobald wir auf die Schaltfläche "Erhitzen" klicken!
        </MDBCardText>
      </>
    }
    else {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Now, let's give an action to our button. We want to make the microwave heat food when we press on it.<br /><br />
          For this, let's click on "New situation" on the card. This will show a modal in which you need to indicate the name of your situation, for example: "Heat".<br /><br />
          Now, our microwave will transition to the "heat" situation when we click on the heat button from the start!
        </MDBCardText>
      </>
    }
  }
}

class Step6 extends React.Component {
  render() {
    if (lang == 'de') {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Wir wollen nun dafür sorgen, dass unsere Mikrowelle eine Rückmeldung anzeigt, wenn sie heizt. Bei einer echten Mikrowelle schaltet sich normalerweise das Innenlicht ein und der Bildschirm zeigt einen Timer an.<br /><br />
          Um dies nachzubilden, werden wir Effekte auf dem Bildschirm und den Innenwänden des Modells hinzufügen. Diese Effekte werden ausgelöst, wenn wir durch Klicken auf die Heiztaste den Zustand "Erhitzen" erreichen.<br /><br />
        </MDBCardText>
      </>
    }
    else {
      return <>
        <MDBCardText className="text-justify" style={style}>
          We now want to make our microwave display feedback when it's heating. Typically, a real microwave would have its internal light turning on and its screen showing a timer.<br /><br />
          To replicate this, we'll be adding effects on the model's screen and internal walls. These effects will be triggered when we reach the "Heat" situation by clicking the heat button.<br /><br />
        </MDBCardText>
      </>
    }
  }
}

class Step7 extends React.Component {
  render() {
    if (lang == 'de') {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Konfigurieren wir zunächst die Lichter.<br />
          Wir wollen die Innenwände der Mikrowelle als Lichtelement definieren. Klicke dazu auf die Innenseite der Mikrowelle. Dabei wird zunächst das Türglas ausgewählt, da es sich vor dem Innenraum befindet.
          Klicke  erneut, um das nächste Element hinter dem aktuellen Element auszuwählen. In diesem Fall wird also bei erneutem Klicken das Innere der Mikrowelle ausgewählt.<br /><br />
          Die Elementauswahl sollte anzeigen, dass das Element "InnerWalls" ausgewählt ist. Wenn das der Fall ist, definiere es als Lichtelement, indem du "Light" (Licht) auswählst.<br /><br />
          Konfiguriere dann die Helligkeit des Lichts ("current emission") und den Farbton ("main color") so, dass es so aussieht, als ob die Mikrowelle von innen mit gelbem Licht beleuchtet wird.
        </MDBCardText>
      </>
    }
    else {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Let's first configure lights.<br />
          We want to define the inside walls of the microwave as a light element. For this, click on the inside of the microwave. You will first select the door glass, as it's in front of the interior.
          Click again to select the next element behind the current one in order to select the microwave's interior.<br /><br />
          The element picker should say that the "InnerWalls" element is selected. If that's the case, define it as a light element by selecting "Light".<br /><br />
          Then, configure the emission and hue so that it looks like the microwave is lit from the inside with a yellow light.
        </MDBCardText>
      </>
    }
  }
}

class Step8 extends React.Component {
  render() {
    if (lang == 'de') {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Konfigurieren wir nun das Display so, dass es anzeigt, dass unsere Mikrowelle heizt. Klicke auf den Bildschirm oben rechts an der Mikrowelle und stelle ihn in der Elementauswahl auf "Display" (Bildschirm).<br /><br />
          Der Einfachheit halber zeigen wir auf dem Bildschirm "heating" anstelle eines herkömmlichen Countdowns an. Dazu lassen wir für den aktuellen Zustand ein Bild auf dem Bildschirm anzeigen.<br /><br />
          Lade das Bild für die Anzeige zunächst <a download href={"./Heating.png"}>hier herunter.</a> Schalte dann die Schaltfläche "Activate display in the current situation" (Bildschirm im aktuellen Zustand aktivieren) ein und lade die gerade heruntergeladene Bilddatei hoch.<br /><br />
          Auf dem Display der Mikrowelle sollte nun "heating" in dem entsprechenden Zustand angezeigt werden.
        </MDBCardText>
      </>
    }
    else {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Now, let's configure the display to show that our microwave is heating. Click on the display, and set it as "Display" on the element picker.<br /><br />
          To keep things simple, we will have our display showing "heating" instead of having a traditional counter, by applying an image on the display for the current situation.<br /><br />
          Download the <a download href={"./Heating.png"}>display image here</a>. Then, switch the "Activate display in the current situation" button on, and upload the file you just downloaded.<br /><br />
          The microwave's display should now display "heating" on the associated situation.
        </MDBCardText>
      </>
    }
  }
}

class Step9 extends React.Component {
  render() {
    if (lang == 'de') {
      return <>
        <MDBCardText className="text-justify" style={style}>
          So langsam sieht es schon sehr gut aus, aber wir sind noch nicht fertig! Bei einer echten Mikrowelle würden sich Licht und Bildschirm ausschalten, wenn der Timer abgelaufen ist. Für uns bedeutet das, dass wir nach einer bestimmten Zeit in den Ruhezustand zurückkehren.<br /><br />
          Wir können angeben, dass unser Zustand zeitabhängig ist, d. h. dass er nach einer bestimmten Zeit in einem anderen Zustand übergeht.<br /><br />
          Klicke dazu in der Situationsliste links auf das Kontextmenü (<i data-test="fa" className="fa fa-ellipsis-v"></i>) des Zustands "Erhitzen" und wähle "Add time-based change" (Zeitabhängigen Übergang hinzufügen). Klicke auf "Proceed" (Fortfahren), leg die gewünschte Zeitspanne fest, z. B. 3000 Millisekunden, und klicke nochmal auf "Proceed" (Fortfahren). Nach der angegebenen Zeit wollen wir zu unserem Ausgangszustand zurückkehren, also wählen wir im Dropdown-Menü den Zustand "Start" für den Übergang und bestätigen dies mit "Finish" (beenden).<br /><br />
        </MDBCardText>
      </>
    }
    else {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Things are starting to look good, but we're not done yet. On a real microwave, the light and display would turn off after the timer is over. For us, this means going back to our idle state after a certain time.<br /><br />
          We can indicate that our situation is time-based, meaning that it will transition to another situation after a given amount of time.<br /><br />
          To do this, click on the contextual menu (<i data-test="fa" className="fa fa-ellipsis-v"></i>) of the "Heat" situation, and select "Add time-based change".
          Click on proceed, and set the amount of time you want, e.g. 5000 milliseconds. We want to transition back to our starting point, so choose the "Start" situation for the transition.<br /><br />
        </MDBCardText>
      </>
    }
  }
}

class Step10 extends React.Component {
  render() {
    if (lang == 'de') {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Um unsere Zustände zu unterscheiden, fügen wir ein weiteres Bild zu unserem Bildschirm hinzu, wenn wir zum "Start"-Zustand zurückkehren.<br /><br />
          Klicke in der Zustandsliste links auf den Zustand "Start" und dann auf den Bildschirm der Mikrowelle. Das Element ist immer noch als Bildschirm konfiguriert, da wir dies zuvor so definiert haben.
          Schalte die Schaltfläche "Activate display in the current situation" (Bildschirm in der aktuellen Situation aktivieren) ein und lade das folgende Bild hoch: <a href={"./Ready.png"} download>Ready-Bild.</a><br /><br />
          Jetzt zeigt unsere Mikrowelle je nach Zustand unterschiedliche Informationen an!
        </MDBCardText>
      </>
    }
    else {
      return <>
        <MDBCardText className="text-justify" style={style}>
          To differentiate our situations, let's add another image to our display when we reach back the "Start" situation.<br /><br />
          Click on the "Start" situation in the situations list, then click on the microwave's display. As you can see, the element is still configured as a display, since we defined it as such earlier.
          Switch the "Activate display in the current situation" button on, and upload the following image: <a href={"./Ready.png"} download>Ready image</a>.<br /><br />
          Now, our microwave displays different information depending on the situation!
        </MDBCardText>
      </>
    }
  }
}

class Step11 extends React.Component {
  render() {
    if (lang == 'de') {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Zum Schluss können wir unsere Konfiguration ausprobieren, um zu prüfen, ob alles funktioniert. Klicke auf die Schaltfläche "Run Test" (Test ausführen) in der oberen rechten Ecke, um den Testmodus zu aktivieren.
          Hier kannst du mit dem Modell interagieren, um Übergänge zwischen den von dir konfigurierten Zuständen auszulösen. Probiere es aus, indem du auf die Schaltfläche "Erhitzen" klickst.<br /><br />
          Dies sollte den Heizzustand auslösen, die Mikrowelle einschalten und "Heating..." auf dem Bildschirm anzeigen. Nach der Zeit, die du im zeitbasierten Übergang konfiguriert hast, sollte sich die Mikrowelle ausschalten und wieder "Ready" anzeigen.<br /><br />
          Wenn sich etwas nicht richtig verhält, kannst du in diesem Tutorial zurückgehen. Du kannst Elemente einfach löschen, indem du auf das Papierkorbsymbol in der Elementauswahl klickst und kannst dann von vorne beginnen.
        </MDBCardText>
      </>
    }
    else {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Lastly, let's try our configuration to check that everything works. Click on the "Run Test" button on the top-right corner to enter the test mode.
          Here, you can interact with your model to trigger transitions between the situations you configured. Try it by clicking the heat button.<br /><br />
          This should trigger the heating situation, light the microwave and show "Heating..." on the display. After the amount of time you configured in the time-based transition, it should turn off and display "Ready" again.<br /><br />
          If something behaves wrongly, feel free to go back in this tutorial. You can easily delete elements by clicking the trash icon in the element picker, and start over.
        </MDBCardText>
      </>
    }
  }
}

class Step12 extends React.Component {
  render() {
    if (lang == 'de') {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Herzlichen Glückwunsch! Du hast es geschafft <span role="img" aria-label="Yeah!">🥳</span><br /><br />
          Du kannst gerne weiter herumspielen und andere Dinge ausprobieren: Erstelle, bearbeite und lösche weitere Zustände und versuche Elemente, mit einem Klick auf das Plus in der Elementauswahl, miteinander zu kombinieren (z.B. Touch und Display, Button und Light...), um komplexere Verhaltensweisen zu konfigurieren.
        </MDBCardText>
      </>
    }
    else {
      return <>
        <MDBCardText className="text-justify" style={style}>
          Congrats! You made it <span role="img" aria-label="Yeah!">🥳</span><br /><br />
          Feel free to play around and try other things: create, edit, delete more situations, try to combine elements together (e.g. touch and display, button and light...) to configure more complex behaviours.
        </MDBCardText>
      </>
    }
  }
}