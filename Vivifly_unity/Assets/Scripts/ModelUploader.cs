﻿using UnityEngine;
using System.Runtime.InteropServices;
using System;
using TriLibCore;
using System.IO;

public class ModelUploader : MonoBehaviour {

    public GameObject uploadedGO;

    /** name of the file loaded during model upload */
    private string fileName = null;

    /** buffer containing the file data during model upload */
    private StringWriter fileContentBase64 = null;

    private void Start()
    {
        if(uploadedGO != null)
        {
            AddColliderToChildren();

            Camera.main.transform.LookAt(uploadedGO.transform);
        }
    }

    /// <summary>
    /// Adds a MeshCollider to all child GameObject
    /// in order to make all parts of the uploaded object clickable
    /// (Should be called everytime after adding an a model)
    /// </summary>
    void AddColliderToChildren() {
        Transform[] children;
        children = this.GetComponentsInChildren<Transform>();
        foreach (Transform child in children) {
            MeshCollider collider = child.gameObject.AddComponent<MeshCollider>();
            collider.convex = false;
            collider.isTrigger = false;
        }
    }

    /// <summary>
    /// Gets the file data from js window and loads the model into the scene
    /// </summary>
    public void StartModelUpload(string fileName) {
        this.fileName = fileName;
        this.fileContentBase64 = new StringWriter();
    }

    /// <summary>
    /// Gets the file data from js window and loads the model into the scene
    /// </summary>
    public void HandleModelDataChunk(string modelDataChunkBase64)
    {
        if (this.fileContentBase64 == null)
        {
            throw new InvalidOperationException("method must only be called after StartModelUpload");
        }

        this.fileContentBase64.Write(modelDataChunkBase64);
    }

    /// <summary>
    /// Gets the file data from js window and loads the model into the scene
    /// </summary>
    public void FinishModelUpload()
    {
        byte[] bytes = Convert.FromBase64String(this.fileContentBase64.ToString());

        var assetLoaderOptions = AssetLoader.CreateDefaultLoaderOptions();

        // Scale down model, since trilib loads it way too big
        assetLoaderOptions.ScaleFactor = 1f;

        var myGameObject = AssetLoader.LoadModelFromStreamNoThread
            (new MemoryStream(bytes), fileName, null, null, this.gameObject, assetLoaderOptions);

        this.uploadedGO = myGameObject.RootGameObject;

        this.AddColliderToChildren();

        Camera.main.transform.LookAt(uploadedGO.transform);
    }
}
