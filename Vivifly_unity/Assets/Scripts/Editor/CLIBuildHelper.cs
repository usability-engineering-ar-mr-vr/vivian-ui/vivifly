using System;
using System.Linq;
using de.ugoe.cs.vivian.core;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Editor
{
    public class CLIBuildHelper : MonoBehaviour
    {
        static void BuildWebGL()
        {
            //This method can be called from cmd/shell
            //Builds a complete WebGL application 
            string[] scenes = {"Assets/Scenes/Main.unity"};
            BuildPipeline.BuildPlayer(scenes, "WebGL", BuildTarget.WebGL, BuildOptions.CompressWithLz4);
        }

    }
}