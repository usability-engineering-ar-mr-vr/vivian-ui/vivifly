﻿using UnityEngine;
using System.Runtime.InteropServices;
using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using de.ugoe.cs.vivian.core;

/**
 * Class to send clicks as JSON to Javascript
 **/
public class MouseClickJSON {
    public string element;
    public float planeX;
    public float planeY;
    public float planeZ;
}

/// <summary>
/// Mouse listener which notifies Javascript if left mouse button is clicked
/// and applies the plane hover effect if activated
/// </summary>
public class MouseController : MonoBehaviour {

    // Import js functions
    [DllImport("__Internal")]
    private static extern void JS_ObjectClicked( string mouseClickJSON);

    /// <summary>
    /// Current gameObject with plane hover effect
    /// </summary>
    private string planeHoverEffectObjectName = null;
    private GameObject lastClickedObject = null;
    private int lastHitCount = 0;
    private IEnumerator stackedObjects;

    /// <summary>
    /// Texture2D for hover effect
    /// </summary>
    public Texture2D hoverTexture;


    void Start() {
        // Stop Capturing keyboard
        WebGLInput.captureAllKeyboardInput = false;
    }

    /// <summary>
    /// Checks for clicks and hover effect
    /// </summary>
    void Update() {
        CheckForClicks();
        CheckPlaneHoverEffect();
    }

    /// <summary>
    /// Checks if the left mouse button was clicked and notifies Javascript
    /// </summary>
    public void CheckForClicks() {
        if (Input.GetMouseButtonDown(0)) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit[] hits = Physics.RaycastAll(ray);
            Vector3 clickedPlane = new Vector3(0, 0, 0);
            List<RaycastHit> effectiveHits = new List<RaycastHit>();

            if ((hits != null) && (hits.Length > 0))
            {
                // select the hits not hitting prototype elements and sort them by distance
                foreach (RaycastHit candidate in hits)
                {
                    if (candidate.collider.GetComponent<VirtualPrototypeElement>() == null)
                    {
                        bool added = false;

                        for (int i = 0; i < effectiveHits.Count; i++)
                        {
                            if (effectiveHits[i].distance > candidate.distance)
                            {
                                effectiveHits.Insert(i, candidate);
                                added = true;
                                break;
                            }
                        }

                        if (!added)
                        {
                            effectiveHits.Add(candidate);
                        }
                    }
                }
            }

            if (effectiveHits.Count > 0) {
                // check if the list of relevant elements changed
                if (!effectiveHits[0].collider.gameObject.Equals(this.lastClickedObject) ||
                    (this.lastHitCount != effectiveHits.Count) ||
                    !this.stackedObjects.MoveNext())
                {
                    // new game object clicked or no further game objects in the click through list -->
                    // we need to restart with the click trough
                    this.lastClickedObject = effectiveHits[0].collider.gameObject;
                    this.lastHitCount = effectiveHits.Count;
                    this.stackedObjects = effectiveHits.GetEnumerator();
                    this.stackedObjects.MoveNext(); // start enumeration
                }

                // Get clicked plane and round by
                clickedPlane = GetPlaneVector((RaycastHit) this.stackedObjects.Current);
            }
            else
            {
                this.lastClickedObject = null;
                this.lastHitCount = 0;
                this.stackedObjects = null;
            }

            // Store click information in JSON string 
            MouseClickJSON clickJSONObject = new MouseClickJSON();
            clickJSONObject.element = this.stackedObjects == null ? "" : ((RaycastHit) this.stackedObjects.Current).transform.name;
            clickJSONObject.planeX = clickedPlane.x;
            clickJSONObject.planeY = clickedPlane.y;
            clickJSONObject.planeZ = clickedPlane.z;
            string clickJSONString = JsonUtility.ToJson(clickJSONObject);

            // Send feedback to Javascript
            JS_ObjectClicked(clickJSONString);
        }
    }

    /// <summary>
    ///  If the plane hover effect is activated this method checks if the mouse is currently above a plane of the hover gameobject
    /// and hightlights the plane
    /// </summary>
    public void CheckPlaneHoverEffect() {
        if (planeHoverEffectObjectName != null) {
            // Remove existing hover effect
            GameObject viviflyCore = GameObject.Find("ViviflyCore");
            ScreensController screensController = viviflyCore.GetComponent<ScreensController>();
            screensController.RemoveScreenEffect(planeHoverEffectObjectName);
            // Add new hover effect
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit)) {
                if (hit.transform && hit.transform.gameObject.name == planeHoverEffectObjectName) {
                    Vector3 hoveredPlane = GetPlaneVector(hit);
                    screensController.displayImage(planeHoverEffectObjectName, hoverTexture, hoveredPlane);

                }
            }

        }
    }

    /// <summary>
    /// Activates the plane hover effect for the given gameObject
    /// </summary>
    /// <param name="gameObjectName">Name of the GameObject whose planes should by highlighted on hover</param>
    public void StartPlaneHoverEffect(string gameObjectName) {
        planeHoverEffectObjectName = gameObjectName;
    }

    /// <summary>
    /// Stops the current plane hover effect
    /// </summary>
    public void StopPlaneHoverEffect() {
        GameObject viviflyCore = GameObject.Find("ViviflyCore");
        ScreensController screensController = viviflyCore.GetComponent<ScreensController>();
        screensController.RemoveScreenEffect(planeHoverEffectObjectName);
        planeHoverEffectObjectName = null;
    }


    /// <summary>
    /// Returns the plane normal vector of a plane that is hit by a RaycastHit 
    /// </summary>
    /// <param name="hit">RaycastHit that hits the plane of a gameObject</param>
    /// <returns>Normal vector of the hit plane</returns>
    private Vector3 GetPlaneVector(RaycastHit hit) {
        Vector3 vector = hit.transform.InverseTransformDirection(hit.normal);
        return new Vector3((float)Math.Round(vector.x, 2), (float)Math.Round(vector.y, 2), (float)Math.Round(vector.z, 2));
    }
}
