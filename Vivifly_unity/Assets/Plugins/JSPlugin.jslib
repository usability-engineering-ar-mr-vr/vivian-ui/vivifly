mergeInto(LibraryManager.library, {

  JS_ObjectClicked: function (mouseClickJSON) {
    var jsonString = Pointer_stringify(mouseClickJSON);
    ReactUnityWebGL.objectClicked(jsonString);
  }
});

mergeInto(LibraryManager.library, {

  JS_StateChanged: function (stateName) {
    ReactUnityWebGL.stateChanged(Pointer_stringify(stateName));
  }
  
});
