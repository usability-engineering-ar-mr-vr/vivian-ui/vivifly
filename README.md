# Vivifly

## Available Scripts for React

After the first checkout, in the project directory, run:
### `npm install`
Downloads the required node packages.

You can then run:
### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run doc`

Builds the application's documentation to the `docs` folder.

## Bridge Between React and Unity
Follow the indications on the [wiki](https://gitlab.com/usability-engineering-ar-mr-vr/vivian-ui/vivifly/-/wikis/Using-the-React-Unity-Bridge).

## Used Libraries

- [MDBootstrap React](https://mdbootstrap.com/docs/react/)
- [React Unity](https://www.npmjs.com/package/react-unity-webgl)
- [React Color](https://casesandberg.github.io/react-color/)
- [react-dropzone](https://github.com/react-dropzone/react-dropzone)
- [react-tooltip](https://www.npmjs.com/package/react-tooltip)
- [Typedoc](https://typedoc.org/)
