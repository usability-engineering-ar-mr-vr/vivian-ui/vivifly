FROM node:18-alpine
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools
RUN apk add --update alpine-sdk
ENV NODE_OPTIONS=--openssl-legacy-provider
WORKDIR /app/src
COPY . ./
RUN npm install
RUN npm run build
RUN npm install -g serve
WORKDIR /app
RUN mv /app/src/build/* .
RUN rm -rf /app/src
EXPOSE 3000
CMD [ "serve", "-s", "/app" ]
